DATA FORMAT

user
  id              ID
  login_name      String
  real_name       String
  email           String
  password_hash   Blob
  active          Boolean

timesheet
  id              ID
  user_id         ID
  name            String
  created         Date
  updated         Date
  min_item_time   Date
  max_item_time   Date

timesheet_item_category
  id              ID
  name            String

timesheet_item
  id              ID
  timesheet_id    ID
  description     String
  time_started    Date
  categories      Object
  {
    // timesheet_item_category_id ID : value String
    category_name String: value String
  }
