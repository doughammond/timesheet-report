// @flow

const _dayFormatter = new window.Intl.DateTimeFormat('en-GB', {
  weekday: 'short',
  day: 'numeric',
  month: 'short',
  year: 'numeric'
});
export function dayFormatter(value: number|string) {
  if (!isNaN((new Date(value)).getTime())) {
    return _dayFormatter.format(value);
  }
  return isNaN(value) ? value : '';
};

const _timeFormatter = new window.Intl.DateTimeFormat('en-GB', {
  hour: 'numeric',
  minute: 'numeric'
});
export function timeFormatter(value: number|string) {
  if (!isNaN((new Date(value)).getTime())) {
    return _timeFormatter.format(value);
  }
  return isNaN(value) ? value : '';
};

type _durationFormatterOptions = {
  daysPerWeek: number,
  hoursPerDay: number,
  maxUnit: string
};

export function durationFormatter(minutes: number, {daysPerWeek = 5, hoursPerDay = 8, maxUnit = 'w'}: _durationFormatterOptions = {}): string {
  let w, d, h, m;

  const mag = ['m', 'h', 'd', 'w'];
  const max = mag.indexOf(maxUnit)

  // console.log('durationFormatter', minutes, daysPerWeek, hoursPerDay, maxUnit, max);

  const f0 = 60, f1 = f0 * hoursPerDay, f2 = f1 * daysPerWeek;

  if (max > 2) {
    w = Math.floor(minutes / f2);
    if (w) {
      minutes -= w * f2;
    }
  }
  if (max > 1) {
    d = Math.floor(minutes / f1);
    if (d) {
      minutes -= d * f1;
    }
  }
  if (max > 0) {
    h = Math.floor(minutes / f0);
    if (h) {
      minutes -= h * f0;
    }
  }

  const _fixIfFrac = (v) => {
    if (!v || v.toFixed(0) === `${v}`) {
      return v;
    } else {
      return v.toFixed(2);
    }
  }

  m = _fixIfFrac(minutes);

  const letters = mag.reverse()
  return [w, d, h, m].reduce(
    (vals, v, i) => {
      if (v) {
        vals.push(`${v}${letters[i]}`);
      }
      return vals;
    },
    []
  ).join(' ');
}

export function splitWorkTimeWithOT(duration: number, nonwork: number, limit: number): Array<number> {
  const workHours = duration - nonwork;
  const ot = Math.max(workHours - limit, 0);
  return [workHours, ot];
}


const route_prefix = process.env.PUBLIC_URL || '';

export function urlForYear(year: number|string): string {
  return `${route_prefix}/year/${year}`;
}
export function urlForQuarter(year: number|string, quarter: number|string): string {
  return `${route_prefix}/quarter/${year}/${quarter}`;
}
export function urlForWeek(year: number|string, week: number|string): string {
  return `${route_prefix}/week/${year}/${week}`;
}

export function titleForYear(year: number|string): string {
  return `${year}`;
}
export function titleForQuarter(year: number|string, quarter: number|string): string {
  return `${year} Q${quarter}`;
}
export function titleForWeek(year: number|string, week: number|string): string {
  return `${year} CW ${week}`;
}

export function titleForUrl(url: string): string {
  const parts = url.split('/');
  // console.log('titleForUrl', parts);
  switch (parts[1]) {
    case 'year':
      return titleForYear(parts[2]);
    case 'quarter':
      return titleForQuarter(parts[2], parts[3]);
    case 'week':
      return titleForWeek(parts[2], parts[3]);
    default:
      return '';
  }
}

export function paramsForUrl(url: string): Object {
  const parts = url.split('/');
  let output = {
    type: parts[1],
    year: parseInt(parts[2], 10),
    week: null,
    quarter: null
  };
  switch (parts[1]) {
    case 'year':
      break;
    case 'quarter':
      output.quarter = parseInt(parts[3], 10);
      break;
    case 'week':
      output.week = parseInt(parts[3], 10);
      break;
    default:
      return {};
  }
  return output;
}