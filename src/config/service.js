// @flow

let cfg = {};


if (process.env.NODE_ENV === 'development') {
  // when running locally under webpack dev server
  cfg = {
    "host": "localhost",
    "port": 5000,
    "path": "/api/v1",
    "secure": false,
    "user": "doug@tts.local",
    "pass": "12341234",
    "use_token": false
  };
}

if (process.env.NODE_ENV === 'production') {
  // when running fully in production
  cfg = {
    "host": "tts.hamsterfight.co.uk",
    "port": 443,
    "path": "/api/v1",
    "secure": true
  };

  // when running locally under the-task-switch-db flask dev server
  // cfg = {
  //   "host": "localhost",
  //   "port": 5000,
  //   "path": "/api/v1",
  //   "secure": false
  // };
}


export const config = cfg;