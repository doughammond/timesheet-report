// @flow

// React core
import React from 'react';
import ReactDOM from 'react-dom';

// Data & State
import { createStore, compose, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

// Sagas
import { RangesSaga } from './data/sagas/Ranges';
import { RoutingSaga } from './data/sagas/Routing';
import { TimesheetRequestSaga, TimesheetAddSaga } from './data/sagas/Timesheets';

// Routing
import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware } from 'connected-react-router';

// Reducers
import { timesheetReducer } from './data/Reducers';

// App Component
import { ConnectedApp } from './components';

// Styles
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.scss';


// Routing History
const route_prefix = process.env.PUBLIC_URL || '';
const history = createBrowserHistory({
  basename: route_prefix
});

// Set up the redux store and middlewares
const sagaMiddleware = createSagaMiddleware();
const middleware = applyMiddleware(
  routerMiddleware(history),
  sagaMiddleware
);
const rootReducer = combineReducers({
  timesheets: timesheetReducer
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  connectRouter(history)(rootReducer),
  composeEnhancers(middleware)
);
sagaMiddleware.run(RangesSaga);
sagaMiddleware.run(RoutingSaga);
sagaMiddleware.run(TimesheetRequestSaga);
sagaMiddleware.run(TimesheetAddSaga);


// Instantiate the app
ReactDOM.render(
  <ConnectedApp
    store={store}
    history={history}
  />,
  document.getElementById('root')
);

// Initialise all timesheets date ranges
store.dispatch({
  type: 'RANGES_REQUEST'
});

// Immediately load data for current URL
store.dispatch({
  type: '@@router/LOCATION_CHANGE',
  payload: history
});