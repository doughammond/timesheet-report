import moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Form,
  FormGroup,
  Col,
  ControlLabel,
  InputGroup,
  FormControl,
  Button,
  Glyphicon
} from 'react-bootstrap';

import './style.scss';

export function _validateRows(rows) {
  const requiredKeys = ['time', 'description'];
  const _valval = (key, val) => {
    switch (key) {
      case 'time':
        return /\d\d:\d\d/.exec(val) !== null
      case 'description':
        return val !== '';
      default:
        return false;
    }
  }
  let error = false;
  for (var idx=0, len=rows.length; idx < len; idx++) {
    let item = rows[idx];
    item._valid = undefined;
    for (var key of requiredKeys) {
      if (!_valval(key, item[key])) {
        item._valid = 'error';
        error = true;
      }
    }
  }
  return !error;
}


class _AddTimesheetForm extends Component {

  componentWillMount() {
    this.reset();
  }

  reset() {
    const date = moment(this.props.date || new Date()).format('YYYY-MM-DD');
    const itemrows = [
      // start with one empty row
      this.createItemRowModel()
    ];
    this.setState({ date, itemrows });
  }

  validate() {
    const isValid = _validateRows(this.state.itemrows);
    this.setState({ itemrows: this.state.itemrows });
    return isValid;
  }

  submit() {
    if (!this.validate()) {
      return false;
    }

    return this.props.dispatch({
      type: 'TIMESHEET_ADD_REQUEST',
      date: this.state.date,
      items: this.state.itemrows.map(
        row => [row.time, row.description, row.classification, row.category]
      )
    });
  }

  onKeyDown(event) {
    // console.log('keydown', event.which);
    if (event.which === 9 && !event.shiftKey) {
      // Tab was pressed, we should add another item row to the form;
      const itemrows = [
        ...this.state.itemrows,
        this.createItemRowModel()
      ];
      this.setState({ itemrows });
    }
  }

  createItemRowModel() {
    return {
      time: '',
      description: '',
      classification: '',
      category: '',
      _valid: undefined
    };
  }

  removeItem(index) {
    // console.log('removeItem', index);
    if (this.state.itemrows.length === 1) {
      return;
    }
    const itemrows = [
      ...this.state.itemrows.slice(0, index),
      ...this.state.itemrows.slice(index+1, this.state.itemrows.length)
    ];
    this.setState({ itemrows });
  }

  setItemValue(key, index, event) {
    // console.log('setItemValue', key, index, event.target.value);
    const itemrow = {
      ...this.state.itemrows[index],
      [key]: event.target.value,
    };
    const itemrows = [
      ...this.state.itemrows.slice(0, index),
      itemrow,
      ...this.state.itemrows.slice(index+1, this.state.itemrows.length)
    ];
    this.setState({ itemrows });
  }

  createItemRowComponents(itemRowModel, index) {
    return (
      <FormGroup
        className="timesheet-item"
        key={index}
        validationState={itemRowModel._valid}
        >
        <InputGroup bsSize="sm">
          <span className="timesheet-item-number">{index + 1}.</span>
          <FormControl
            onChange={this.setItemValue.bind(this, 'time', index)}
            className="timesheet-item-time"
            value={itemRowModel.time}
            type="time"
            />
          <FormControl
            onChange={this.setItemValue.bind(this, 'description', index)}
            className="timesheet-item-description"
            value={itemRowModel.description}
            type="text"
            placeholder="Description"
            />
          <FormControl
            onChange={this.setItemValue.bind(this, 'classification', index)}
            className="timesheet-item-classification"
            value={itemRowModel.classification}
            type="text"
            placeholder="Qn"
            />
          <FormControl
            onChange={this.setItemValue.bind(this, 'category', index)}
            onKeyDown={this.onKeyDown.bind(this)}
            className="timesheet-item-category"
            value={itemRowModel.category}
            type="text"
            placeholder="Category"
            />
            {
              (index !== 0) ?
                <Button
                  onClick={this.removeItem.bind(this, index)}
                  className="remove-button"
                  bsSize="small"
                  bsStyle="link">
                  <Glyphicon glyph="remove" />
                </Button>
              : <span className="remove-button-space" />
            }
        </InputGroup>
      </FormGroup>
    );
  }

  setDate(event) {
    this.setState({ date: event.target.value });
  }

  render() {
    return (
      <Form horizontal className="add-timesheet-form">
        <FormGroup controlId="timesheet-date">
          <Col componentClass={ControlLabel} sm={3}>
            Timesheet Date
          </Col>
          <Col sm={9}>
            <FormControl
              onChange={this.setDate.bind(this)}
              type="date"
              value={this.state.date}
              />
          </Col>
        </FormGroup>
        <div className="timesheet-items-container">
        {
          this.state.itemrows.map(
            (itemRowModel, index) => this.createItemRowComponents(itemRowModel, index)
          )
        }
        </div>
      </Form>
    );
  }
}

function mapStateToProps(state) {
  return {
    date: state.formDate
  };
}

export const AddTimesheetForm = connect(
  mapStateToProps,
  undefined, undefined,
  { withRef: true }
)(_AddTimesheetForm);
