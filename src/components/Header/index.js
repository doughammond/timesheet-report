import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';

import { AddTimesheetDialog } from '../AddTimesheetDialog';
import { dayFormatter, titleForUrl } from '../../Utils';

import './style.scss';


class DateRange extends Component {
  render() {
    const { minDate, maxDate } = this.props;
    if (minDate !== Infinity && maxDate !== -Infinity) {
      return (
        <small>
          { dayFormatter(minDate) }
          { " - " }
          { dayFormatter(maxDate) }
        </small>
      );
    }
    return null;
  }
}

class _Header extends Component {
  render() {
    return (
      <Row className="header">
        <Col sm={12}>
          <h2>
            Timesheet Report
            <small>
              { titleForUrl(this.props.router.location.pathname) }
              <DateRange
                minDate={this.props.minDate}
                maxDate={this.props.maxDate}
              />
            </small>
          </h2>
        </Col>
        <Col sm={11}>
          {this.props.children}
        </Col>
        <AddTimesheetDialog />
      </Row>
    );
  }
}


function mapStateToProps(state) {
  // console.log('Header mapStateToProps', state);
  return {
    minDate: state.timesheets.sheet.minDate,
    maxDate: state.timesheets.sheet.maxDate,

    // we map the router to props so that we can update the title
    router: state.router
  };
};

export const Header = connect(
  mapStateToProps
)(_Header);
