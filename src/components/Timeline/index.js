import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as d3 from 'd3';
import * as d3s from 'd3-scale';
import * as d3t from 'd3-time';
import * as d3tf from 'd3-time-format';
import * as d3a from 'd3-axis';
import * as d3b from 'd3-brush';

import './style.scss';


class _Timeline extends Component {
  render() {
    const { params } = this;
    const width =  params.width  + params.margins[1] + params.margins[3];
    const height = params.height + params.margins[0] + params.margins[2];

    return (
      <svg id="timeline-chart" className="chart" width={width} height={height}>
        <g id="main" className="main" width={width} height={params.main.height} transform={`translate(${params.margins[3]}, ${params.margins[0]})`}>
          <g id="main-x-axis" className="x1 axis main-x-axis" />
          <g id="main-lanes" className="main-lanes" />
          <g id="main-text" className="main-text" />
          <g id="main-items" className="main-items" />
        </g>
        <g id="mini" className="mini" width={width} height={params.mini.height} transform={`translate(${params.margins[3]}, ${params.main.height + params.margins[0]})`}>
          <g id="mini-lanes" className="mini-lanes" />
          <g id="mini-text" className="mini-text" />
          <g id="mini-items" className="mini-items" />
          <g id="zoom-select" className="x brush" />
        </g>
      </svg>
    );
  }

  get params() {
    const [timeBegin_t, timeEnd_t, lanes, items] = this.props.timelineData;
    const laneLength = lanes.length;

    const one_day = 24 * 60 * 60 * 1000;
    const timeBegin = new Date(timeBegin_t);
    const timeEnd = new Date(timeEnd_t + one_day);

    const m = [20, 80, 20, 30]; // magic numbers to make the graphics fit :/
    const w = 960 - m[1] - m[3];
    const h = 400 - m[0] - m[2];
    const miniLaneHeight = 4;
    const miniHeight = laneLength * miniLaneHeight;
    const mainHeight = h - miniHeight;

    return {
      margins: m,
      width: w,
      height: h,
      main: {
        height: mainHeight,
        laneHeight: mainHeight / laneLength,
        laneStart: 8
      },
      mini: {
        height: miniHeight,
        laneHeight: miniLaneHeight,
        laneStart: 16
      },
      time: {
        begin: timeBegin,
        end: timeEnd
      },
      lanes,
      items
    };
  }

  get scales_axes() {
    return this._scales_axes;
  }

  _create_scale_axes() {
    const { params } = this;

    const x_scale_main = d3s.scaleTime()
             .domain([params.time.begin, params.time.end])
             .range([
                params.margins[3],
                params.width - params.margins[3] - params.margins[1]
              ]);

    const x_scale_mini = d3s.scaleTime()
            .domain([params.time.begin, params.time.end])
             .range([
                params.margins[3],
                params.width - params.margins[3] - params.margins[1]
              ]);

    const y_scale_main = d3s.scaleLinear()
             .domain([0, params.lanes.length])
             .range([0,  params.main.height]);

    const y_scale_mini = d3s.scaleLinear()
             .domain([0, params.lanes.length])
             .range([0,  params.mini.height]);

    const formatMillisecond = d3tf.timeFormat(".%L"),
          formatSecond = d3tf.timeFormat(":%S"),
          formatMinute = d3tf.timeFormat("%H:%M"),
          formatHour = d3tf.timeFormat("%H:00"),
          formatDay = d3tf.timeFormat("%a %e"),
          formatWeek = d3tf.timeFormat("%b %e"),
          formatMonth = d3tf.timeFormat("%B"),
          formatYear = d3tf.timeFormat("%Y");

    function multiFormat(date) {
      return (d3t.timeSecond(date) < date ? formatMillisecond
            : d3t.timeMinute(date) < date ? formatSecond
            : d3t.timeHour(date) < date ? formatMinute
            : d3t.timeDay(date) < date ? formatHour
            : d3t.timeMonth(date) < date ? (d3t.timeWeek(date) < date ? formatDay : formatWeek)
            : d3t.timeYear(date) < date ? formatMonth
            : formatYear)(date);
    }

    const x_axis_main = d3a.axisTop()
                 .scale(x_scale_main)
                 .tickFormat(multiFormat)
                 .tickSize(5)
                 .tickPadding(1);

    this._scales_axes = {
      x_scale_main, x_scale_mini,
      y_scale_main, y_scale_mini,

      x_axis_main
    };
  }

  componentDidMount() {
    this._create_scale_axes();
  }

  componentWillUpdate() {
    const { params } = this;

    const { x_scale_main, x_scale_mini, y_scale_main, y_scale_mini, x_axis_main } = this.scales_axes;

    const chart = d3.select("#timeline-chart");
    const main = chart.select("#main");
    const mini = chart.select("#mini");

    // update the scale domains
    x_scale_mini.domain([params.time.begin, params.time.end]);
    chart.select('#main-x-axis').call(x_axis_main);

    //main lane lines
    main.select('#main-lanes').selectAll('line')
      .data([...params.lanes, ''])
      .enter()
      .append("line")
        .attr("x1", params.margins[3])
        .attr("x2", params.width - params.margins[1] - params.margins[3])
        .attr("y1", (lane, i) => y_scale_main(i) + params.main.laneStart)
        .attr("y2", (lane, i) => y_scale_main(i) + params.main.laneStart)
        .attr("stroke", "lightgray");

    // main lanes text
    main.select('#main-text').selectAll('text')
      .data(params.lanes)
      .enter()
      .append("text")
      .text(d => d)
      .attr("x", 25)
      .attr("y", (lane, i) => y_scale_main(i + .5) + params.main.laneStart)
      .attr("dy", ".5ex")
      .attr("text-anchor", "end")
      .attr("class", "laneText");

    // mini lane lines
    mini.select('#mini-lanes').selectAll('line')
      .data([...params.lanes, ''])
      .enter()
      .append("line")
        .attr("x1", params.margins[3])
        .attr("x2", params.width - params.margins[1] - params.margins[3])
        .attr("y1", (lane, i) => y_scale_mini(i) + params.mini.laneStart)
        .attr("y2", (lane, i) => y_scale_mini(i) + params.mini.laneStart)
        .attr("stroke", "lightgray");

    //mini item blocks
    const mini_items = mini.select('#mini-items').selectAll('rect')
      .data(params.items);

    // create new
    mini_items.enter().append("rect");
    // remove excess
    mini_items.exit().remove();
    // update attributes
    mini_items.attr("class", d => `item-${d.classification}`)
              .attr("x",     d => x_scale_mini(d.start))
              .attr("y",     d => y_scale_mini(d.lane) + params.mini.laneStart)
              .attr("width", d => x_scale_mini(d.end) - x_scale_mini(d.start))
              .attr("height", params.mini.laneHeight);

    // brush for zoom selection mask
    const x_mini_range = x_scale_mini.range();
    const y_mini_range = y_scale_mini.range();
    const brush = d3b.brushX()
                .extent([
                  [x_mini_range[0], y_mini_range[0] + params.mini.laneStart - 2],
                  [x_mini_range[1], y_mini_range[1] + params.mini.laneStart + 2]
                ])
                .on("brush", () => this._update_main_display(params, main, chart));

    mini.select("#zoom-select").call(brush);

    // do main render
    this._update_main_display(params, main, chart);
  }

  _update_main_display(params, main, chart) {
    const { x_scale_mini, x_scale_main, y_scale_main, x_axis_main } = this.scales_axes;

    const evt = d3.event;
    const minExtent = (evt && evt.selection) ? x_scale_mini.invert(evt.selection[0]) : params.time.begin;
    const maxExtent = (evt && evt.selection) ? x_scale_mini.invert(evt.selection[1]) : params.time.end;
    const visItems = params.items.filter(d => d.start < maxExtent && d.end > minExtent);

    // update the main time axis
    x_scale_main.domain([minExtent, maxExtent]);
    chart.select('#main-x-axis').call(x_axis_main);

    // update the main items
    const rects = main.select('#main-items').selectAll('rect').data(visItems);

    // add new
    rects.enter().append("rect");
    // remove excess
    rects.exit().remove();
    // update properties
    rects.attr("id",     d => `item-${d.id}`)
         .attr("class",  d => `item-${d.classification}`)
         .attr("x",      d => x_scale_main(d.start))
         .attr("y",      d => y_scale_main(d.lane) + params.main.laneStart)
         .attr("width",  d => x_scale_main(d.end) - x_scale_main(d.start))
         .attr("height", params.main.laneHeight);
  }
}

export const Timeline = connect(
  state => ({
    timelineData: state.timesheets.sheet.laneData
  })
)(_Timeline);