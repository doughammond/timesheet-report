import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Col } from 'react-bootstrap';

import { durationFormatter } from '../../Utils';

import './style.scss';


class DurationCell extends Component {
  render() {
    if (this.props.duration) {
      return (<div className="cell">
        <span className="duration">{ this.props.duration }</span>
        <span className="percentage">{ this.props.percentage }%</span>
      </div>);
    }
    return <div />;
  }
}

export const pivotTemplates = {
  duration: function(val, data) {
    const pc = ((val / data.duration) * 100).toFixed(2);
    const dur = durationFormatter(val);
    return <DurationCell duration={dur} percentage={pc} />;
  },
  _default: function(val, data) {
    return <div className="cell">{val}</div>;
  }
}

class _PivotTable extends Component {
  render() {
    const { pivotData, pivotValues, dimensions, calculatedProperty, template } = this.props;
    if (!pivotData) {
      return (<table></table>);
    }

    const title = [...dimensions].reverse().map(d => d.title).join(' / ');
    const dim1v = dimensions.length === 2 ? pivotValues[dimensions[1].value] : [];

    return (
      <Col sm={12}>
      <table className="PivotTable">
        <thead>
          <tr>
            <th className="dim0">
              {title}
            </th>
            {
              [...pivotValues[dimensions[0].value]].sort().map(
                d1 => <th className="dim0" key={d1}>{d1}</th>
              )
            }
            <th className="total">Totals</th>
          </tr>
        </thead>
        <tbody>
          {
            [...dim1v].sort().map(
              d2 => (<tr key={d2}>
                <td key={d2 + '.key'} className="dim1">{d2}</td>
                {
                  [...pivotValues[dimensions[0].value]].sort().map(
                    d1 => (<td className="value" key={d1 + '.' + d2 + '.value'}>{
                      template(
                        pivotData['children'][d1]['children'][d2][calculatedProperty],
                        pivotData
                      )
                    }</td>)
                  )
                }
                <td className="total" key={d2+'.total'}>
                {
                  template(
                    [...pivotValues[dimensions[0].value]].reduce(
                      (d1_acc, d1) => {
                        d1_acc += pivotData['children'][d1]['children'][d2][calculatedProperty] || 0;
                        return d1_acc;
                      },
                      0
                    ),
                    pivotData
                  )
                }
                </td>
              </tr>)
            )
          }
          <tr className="total-row">
            <th className="total">Totals</th>
            {
              [...pivotValues[dimensions[0].value]].sort().map(
                d1 => (<td className="total" key={d1+'.total'}>{
                  template(
                    pivotData['children'][d1][calculatedProperty],
                    pivotData
                  )
                }</td>)
              )
            }
            <td className="grand-total" key="total">
            {
              template(pivotData[calculatedProperty], pivotData)
            }
            </td>
          </tr>
        </tbody>
      </table>
      </Col>
    );
  }
}


function mapStateToProps(state) {
  // console.log('PivotTable mapStateToProps', state);
  const dimensions = state.timesheets.pivotDimensions;
  const calculatedProperty = 'duration';
  let nextProps = {
    dimensions,
    calculatedProperty,
    template: pivotTemplates[calculatedProperty] || pivotTemplates._default
  };
  [nextProps['pivotData'], nextProps['pivotValues']] = state.timesheets.sheet.calculatePivot(
    nextProps.dimensions,
    nextProps.calculatedProperty
  );
  return nextProps;
};


export const PivotTable = connect(
  mapStateToProps
)(_PivotTable);
