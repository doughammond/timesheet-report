import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar as BarChart, Pie as PieChart } from 'react-chartjs-2';
import { Col } from 'react-bootstrap';

import { splitWorkTimeWithOT, durationFormatter } from '../../Utils';

import './style.scss';


const commonChartOptions = {
  responsive: true,
  maintainAspectRatio: false
};

const workClassificationColours = {
  'Q1': '#E76F51',
  'Q2': '#F4A261',
  'Q3': '#E9C46A',
  'Q4': '#264653'
};

export function _calculate_ClassificationPieChart(pivotData, pivotValues) {
  const categories = [...pivotValues.classification].sort();
  const data = {
    labels: categories,
    datasets: [{
      data: categories.map(
        // category name => category duration in minutes
        category => pivotData['children'][category]['duration']
      ).map(
        // minutes => percent of total
        minutes => 100 * (minutes / pivotData['duration'])
      ),
      backgroundColor: categories.map(
        category => workClassificationColours[category]
      )
    }]
  };
  const options = {
    ...commonChartOptions,
    title: {
      display: true,
      text: 'Work Classification'
    }
  };
  return { categories, data, options };
}

export function _calculate_LengthSegmentsDurationPerDay(weekdayStats) {
  const categories = Object.keys(weekdayStats);
  const dataPoints = Object.keys(weekdayStats).map(weekday => {
    const stat = weekdayStats[weekday];
    return [
      // average day duration
      stat['duration'] / stat['daycount'],
      // average number of items in each work day
      stat['itemcount'] / stat['daycount'],
      // average item duration in each work day
      stat['duration'] / stat['itemcount']
    ];
  });
  const data = {
    labels: categories,
    datasets: [
      {
        label: 'average day length',
        data: dataPoints.map(d => d[0]),
        backgroundColor: '#E76F51',
        yAxisID: 'y-0'
      },
      {
        label: 'average segment count',
        data: dataPoints.map(d => d[1]),
        backgroundColor: '#F4A261',
        yAxisID: 'y-1'
      },
      {
        label: 'average segment duration',
        data: dataPoints.map(d => d[2]),
        backgroundColor: '#E9C46A',
        yAxisID: 'y-2'
      }
    ]
  };
  const options = {
    ...commonChartOptions,
    title: {
      display: true,
      text: 'Distribution of day length, segment duration and count per day'
    },
    scales: {
      yAxes: [
        { id: 'y-0', position: 'left', display: false, ticks: { min: 0 } },
        { id: 'y-1', position: 'left', ticks: { min: 0 } },
        { id: 'y-2', position: 'right', ticks: { min: 0 } },
      ]
    },
    tooltips: {
      callbacks: {
        label: function(tooltip, data) {
          // tooltip: {
          //     // X Value of the tooltip as a string
          //     xLabel: String,

          //     // Y value of the tooltip as a string
          //     yLabel: String,

          //     // Index of the dataset the item comes from
          //     datasetIndex: Number,

          //     // Index of this data item in the dataset
          //     index: Number
          // }
          let val = tooltip.yLabel;
          const dsi = tooltip.datasetIndex;
          switch (dsi) {
            case 0:
              let [work, ot] = splitWorkTimeWithOT(val, 60, 8*60);
              work = durationFormatter(work - ot);
              if (ot) {
                ot = durationFormatter(ot);
                val = `${work} + ${ot} OT`;
              } else {
                val = `${work}`;
              }
              break;
            case 2:
              val = durationFormatter(val);
              break;
            default:
              break;
          }
          return `${data.datasets[dsi].label}: ${val}`;
        }
      }
    }
  };
  return { categories, data, options };
}

export function _calculate_ClassificationsPerDay(weekdayStats) {
  const classifications = ['Q1', 'Q2', 'Q3', 'Q4'];
  const categories = Object.keys(weekdayStats);
  const dataPoints = classifications.map(classification => {
    return categories.map(
      weekday => (weekdayStats[weekday].classifications[classification] || 0) / weekdayStats[weekday]['duration']
    )
  });
  const data = {
    labels: categories,
    datasets: classifications.map(
      (classification, index) => ({
        label: classification,
        data: dataPoints[index],
        backgroundColor: workClassificationColours[classification],
        yAxisID: 'y-0'
      })
    )
  };
  const options = {
    ...commonChartOptions,
    title: {
      display: true,
      text: 'Distribution of classifications per day'
    },
    scales: {
      xAxes: [{
        stacked: true
      }],
      yAxes: [{
        id: 'y-0',
        position: 'left',
        ticks: { min: 0 },
        stacked: true
      }]
    }
  };
  return { categories, data, options };
}

export function _calculate_PlannedWorkDistribution(distribution) {

  const categories = [...(new Set([
    ...Object.keys(distribution['segments']),
    ...Object.keys(distribution['tasks'])
  ]))].map(
    v => parseInt(v, 10)
  ).sort(
    (a, b) => {
      return a - b;
    }
  );

  const data = {
    labels: categories.map(durationFormatter),
    datasets: [
      {
        label: 'task count',
        data: categories.map(d => distribution['tasks'][d] || 0),
        backgroundColor: '#E76F51',
        yAxisID: 'y-0',
      },
      {
        label: 'work time segments',
        data: categories.map(d => distribution['segments'][d] || 0),
        backgroundColor: '#F4A261',
        yAxisID: 'y-0',
      }
    ]
  };
  const options = {
    ...commonChartOptions,
    title: {
      display: true,
      text: 'Distribution of planned-work (duration)'
    },
    scales: {
      yAxes: [
        { id: 'y-0', position: 'left' },
      ]
    }
  };
  return { categories, data, options };
}

class _PlannedWorkDurationDistribution extends Component {
  render() {
    const { weekdayStats, distribution, pivotData, pivotValues } = this.props;

    const classificationPie = _calculate_ClassificationPieChart(pivotData, pivotValues);
    const classificationsPerDay = _calculate_ClassificationsPerDay(weekdayStats);
    const countsPerDay = _calculate_LengthSegmentsDurationPerDay(weekdayStats);
    const plannedWorkDistribution = _calculate_PlannedWorkDistribution(distribution);

    return (
      <Col sm={12} className="planned-work-duration-distribution foo">
        <PieChart
          data={classificationPie.data}
          options={classificationPie.options}
          height={300}
          width={300}
          />
        <BarChart
          data={classificationsPerDay.data}
          options={classificationsPerDay.options}
          height={300}
          />
        <BarChart
          data={countsPerDay.data}
          options={countsPerDay.options}
          height={300}
          />
        <BarChart
          data={plannedWorkDistribution.data}
          options={plannedWorkDistribution.options}
          height={300}
          />
      </Col>
    );
  }
}


function mapStateToProps(state) {
  const dimensions = [
    { value: 'classification', title: 'Classification' },
    { value: 'category', title: 'Category' }
  ];
  const [pivotData, pivotValues] = state.timesheets.sheet.calculatePivot(
    dimensions,
    'duration'
  );
  // console.log('PlannedWorkDurationDistribution mapStateToProps', pivotData, pivotValues);
  return {
    weekdayStats: state.timesheets.sheet.weekdayStats,
    distribution: state.timesheets.sheet.distribution,
    pivotData,
    pivotValues
  };
};

export const PlannedWorkDurationDistribution = connect(
  mapStateToProps
)(_PlannedWorkDurationDistribution);
