import React, { Component } from 'react';

import { dayFormatter, durationFormatter } from '../../Utils';

import TimesheetItem from '../TimesheetItem';

import './style.scss';


export default class TimesheetDay extends Component {
  render() {
    return (
      <tbody className="timesheet-day">
        <tr>
          <td className="date">
            { dayFormatter(this.props.model.date) }
          </td>
          <td colSpan="4" className="total">
            { durationFormatter(this.props.model.duration, { maxUnit: 'h' }) }
          </td>
        </tr>
        {
          this.props.model.items.map(
            (item, i) => <TimesheetItem key={i} model={item} />
          )
        }
      </tbody>
    );
  }
}
