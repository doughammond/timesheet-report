import React, { Component } from 'react';

import TimesheetDay from '../TimesheetDay';

import './style.scss';


export class TimesheetDetail extends Component {
  render() {
    return (
      <table className="timesheet-detail">
      {
        this.props.days.map(
          day => <TimesheetDay key={day.date} model={day} />
        )
      }
      </table>
    );
  }
}


