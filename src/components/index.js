// React core
import React, { Component } from 'react';

// Redux
import { connect, Provider } from 'react-redux';

// Components
import { Grid, Modal, ProgressBar } from 'react-bootstrap';
import { Header } from './Header';
import { ConnectedTimesheetReport } from './TimesheetReport';
import { Links } from './Links';


// Routing
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';


// Main App
export class _App extends Component {
  render() {
    return (
      <Grid fluid={true}>
        <Header>
          <Links />
        </Header>
        {this.props.children}
        <Modal
          show={this.props.loading}
          backdrop="static"
          >
            <ProgressBar
              active={true}
              striped={true}
              min={0}
              max={1}
              now={1}
              label={this.props.message}
            />
        </Modal>
      </Grid>
    );
  }
}

const App = connect(
  (state) => {
    return {
      loading: state.timesheets.loading,
      message: state.timesheets.loading_message
    }
  }
)(_App);

// Placeholder component
export class Instructions extends Component {
  render() {
    return (
      <div><p>Please select a Year, Quarter or Week above</p></div>
    );
  }
}


export class ConnectedApp extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <ConnectedRouter history={this.props.history}>
          <App>
            <Switch>
              <Route
                path="/year/:year"
                component={ConnectedTimesheetReport}
              />

              <Route
                path="/quarter/:year/:quarter"
                component={ConnectedTimesheetReport}
              />

              <Route
                path="/week/:year/:week"
                component={ConnectedTimesheetReport}
              />

              <Route
                component={Instructions}
              />
            </Switch>
          </App>
        </ConnectedRouter>
      </Provider>
    );
  }
}
