import React, { Component } from 'react';

import { timeFormatter, durationFormatter } from '../../Utils';

import './style.scss';


export default class TimesheetItem extends Component {
  render() {
    return (
      <tr className="timesheet-item">
        <td className="time">{ timeFormatter(this.props.model.time) }</td>
        <td className="duration">{ durationFormatter(this.props.model.duration, { maxUnit: 'h' }) }</td>
        <td className="classification">{ this.props.model.classification }</td>
        <td className="category">{ this.props.model.category }</td>
        <td className="description">{ this.props.model.description }</td>
      </tr>
    );
  }
}
