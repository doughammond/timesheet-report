import moment from 'moment';

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Col } from 'react-bootstrap';

import { TimesheetDetail } from '../TimesheetDetail';

import {
  durationFormatter,
  splitWorkTimeWithOT,
  urlForYear,
  urlForQuarter,
  urlForWeek
} from '../../Utils';


import './style.scss';


class _WeeklySummaryRow extends Component {

  handleToggle() {
    this.props.dispatch({
      type: 'TOGGLE_WEEK_DETAIL',
      week_code: this.props.weekInfo.code
    });
  }

  render() {
    const week = this.props.weekInfo;
    const week_detail = this.props.week_detail || {};
    const expand = !!week_detail[week.code];
    const expandClass = expand ? 'expand' : '';

    const nonWorkTimeTotal = week.days.map(
      d => d.items.reduce(
        (dayNonWorkTotal, i) => (i.classification === 'Q4') ? dayNonWorkTotal + i.duration : dayNonWorkTotal,
        0
      )
    ).reduce(
      (weekNonWorkTotal, dayNonWorkTotal) => weekNonWorkTotal + dayNonWorkTotal,
      0
    );

    const [totalHours, ot] = splitWorkTimeWithOT(
      week.hours,
      nonWorkTimeTotal, // should add up to 1 hr for lunch * 5 days
      40*60 // 40 hr working week
    );

    const dayTotals = week.days.reduce(
      (totals, day) => {
        totals[moment(day.date).format('E')] = day.duration;
        return totals;
      },
      {}
    );

    return (
      <tbody>
        <tr>
          <td className="cw">
            <div className="detail-expand" onClick={() => this.handleToggle()}>{ expand ? '\u23F7' : '\u23F5' }</div>
            <div>
              <div className="week-number">
                <Link to={urlForYear(week.yearOf)}>{week.yearOf}</Link>
                {' / '}
                <Link to={urlForQuarter(week.yearOf, week.quarterOf)}>Q{week.quarterOf}</Link>
                {' / '}
                <Link to={urlForWeek(week.yearOf, week.weekOf)}>CW {week.weekOf}</Link>
              </div>
              <span className="date-range">{ week.startDate } - { week.endDate }</span>
            </div>
          </td>
          <td>{ durationFormatter(dayTotals[1] || 0, { maxUnit: 'h' }) }</td>
          <td>{ durationFormatter(dayTotals[2] || 0, { maxUnit: 'h' }) }</td>
          <td>{ durationFormatter(dayTotals[3] || 0, { maxUnit: 'h' }) }</td>
          <td>{ durationFormatter(dayTotals[4] || 0, { maxUnit: 'h' }) }</td>
          <td>{ durationFormatter(dayTotals[5] || 0, { maxUnit: 'h' }) }</td>
          <td>{ durationFormatter(dayTotals[6] || 0, { maxUnit: 'h' }) }</td>
          <td>{ durationFormatter(dayTotals[7] || 0, { maxUnit: 'h' }) }</td>
          <td className="duration">
            <span className="work-time" key="work-time">{
              durationFormatter(totalHours, { maxUnit: 'h'})
            }</span>
          </td>
          <td className="duration">{
            ot ?
              <span className="over-time" key="over-time">{
                durationFormatter(ot, { maxUnit: 'h'})
              }</span>
            :
              ''
          }</td>
        </tr>
        <tr className={"detail " + expandClass }>
          <td colSpan="10">
            <TimesheetDetail days={week.days} />
          </td>
        </tr>
      </tbody>
    );
  }
}

function mapStateToProps_WeeklySummaryRow(state) {
  return {
    week_detail: state.timesheets.week_detail
  }
}

const WeeklySummaryRow = connect(
  mapStateToProps_WeeklySummaryRow
)(_WeeklySummaryRow);

function groupDaysByWeek(days) {
  const weeks = {};
  for (let day of days) {
    let [yearOf, weekOf] = day.week;
    let wd = moment(`${yearOf}W${weekOf}`);
    let quarterOf = wd.clone().quarter();
    let weekCode = `${yearOf}-${weekOf}`;
    if (!(weekCode in weeks)) {
      weeks[weekCode] = {
        code: weekCode,
        yearOf, weekOf, quarterOf,
        startDate: wd.clone().startOf('week').format('D MMM YYYY'),
        endDate: wd.clone().endOf('week').format('D MMM YYYY'),
        hours: 0,
        days: []
      };
    }
    weeks[weekCode].hours += day.duration;
    weeks[weekCode].days.push(day);
  }
  const sortedCodes = Object.keys(weeks);
  return sortedCodes.map(
    weekCode => weeks[weekCode]
  );
}


export class WeeklySummary extends Component {
  render() {
    return (
      <Col sm={12}>
      <table className="weekly-summary">
        <thead>
          <tr>
            <th>CW</th>
            <th>M</th>
            <th>T</th>
            <th>W</th>
            <th>T</th>
            <th>F</th>
            <th>S</th>
            <th>S</th>
            <th>Week Hours</th>
            <th>OT</th>
          </tr>
        </thead>
          {
            groupDaysByWeek(this.props.sheet.days).map(
              weekInfo => <WeeklySummaryRow weekInfo={weekInfo} key={weekInfo.code} />
            )
          }
      </table>
      </Col>
    );
  }
}
