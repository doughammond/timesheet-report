import moment from 'moment';

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';

import {
  urlForYear,
  urlForQuarter,
  urlForWeek
} from '../../Utils';

import './styles.scss';


export function generateYearQuarterWeeks(start, end) {
  const dates = {};
  let current = moment(start);
  while (current.toDate() < end) {
    let y = current.year();
    let q = current.quarter();
    let w = current.isoWeek();
    if (!(y in dates)) {
      dates[y] = {};
    }
    if (!(q in dates[y])) {
      dates[y][q] = [];
    }
    dates[y][q].push(w);
    current = current.add(1, 'week');
  }
  return dates;
}

export class LinkSetQuarter extends Component {
  render() {
    const { year, quarter, weeks } = this.props;
    return (
      <div className="links-quarter">
        <Link
          className="link-quarter"
          to={urlForQuarter(year, quarter)}
        >
          {`Q${quarter}`}
        </Link>
        {
          weeks.map(
            week => (
              <Link
                className="link-week"
                key={year+'-'+week}
                to={urlForWeek(year, week)}
              >
                {week}
              </Link>
            )
          )
        }
      </div>
    );
  }
}

export class LinkSetYear extends Component {
  render() {
    const { year, yearData } = this.props;
    const quarters = Object.keys(yearData);

    return (
      <Col sm={5} className="links-year">
        <Link
          className="link-year"
          to={urlForYear(year)}
        >
          {year}
        </Link>
        {
          quarters.map(
            quarter => (
              <LinkSetQuarter
                key={year+'-'+quarter}
                year={year}
                quarter={quarter}
                weeks={yearData[quarter]}
              />
            )
          )
        }
      </Col>
    );
  }
}

export class LinkSet extends Component {
  render() {
    const dates = generateYearQuarterWeeks(
      this.props.minDate,
      this.props.maxDate
    );

    return (
      <Row className="links-all">
        {
          Object.keys(dates).map(
            year => (
              <LinkSetYear
                key={year}
                year={year}
                yearData={dates[year]}
              />
            )
          )
        }
      </Row>
    );
  }
}

function mapStateToProps(state) {
  // console.log('Links mapStateToProps', state);
  return {
    minDate: state.timesheets.minDate,
    maxDate: state.timesheets.maxDate,

    // we map the routing to props so that the Link elements
    // update their active class when the route changes.
    routing: state.routing
  };
};

export const Links = connect(
  mapStateToProps
)(LinkSet);
