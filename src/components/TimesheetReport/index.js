import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Panel } from 'react-bootstrap';

import { PivotTable } from '../PivotTable';
import { Timeline } from '../Timeline';
import { PlannedWorkDurationDistribution } from '../PlannedWorkDurationDistribution';
import { WeeklySummary } from '../WeeklySummary';

import './style.scss';


export class TimesheetReport extends Component {
  render() {
    const numDays = this.props.sheet.allData.length;
    if (numDays === 0) {
      return (
        <Row className="timesheet-report">
          <Col sm={12}>
            <Panel bsStyle="warning">No data for this period !</Panel>
          </Col>
        </Row>
      );
    } else {
      return (
        <Row className="timesheet-report">
          <PivotTable />
          <Timeline />
          <PlannedWorkDurationDistribution />
          <WeeklySummary
            sheet={this.props.sheet}
          />
        </Row>
      );
    }
  }
}


function mapStateToProps(state) {
  // console.log('TimesheetReport mapStateToProps', state);
  return {
    sheet: state.timesheets.sheet
  }
};

export const ConnectedTimesheetReport = connect(
  mapStateToProps, dispatch => ({ dispatch }) 
)(TimesheetReport);
