import React, { Component } from 'react';
import { Button, Modal, Col } from 'react-bootstrap';

import { AddTimesheetForm } from '../AddTimesheetForm';


export class AddTimesheetDialog extends Component {
  constructor() {
    super();
    this.state = {
      showModal: false
    };
  }

  close() {
    this.setState({ showModal: false });
  }

  reset() {
    this.refs['add-timesheet-form'].getWrappedInstance().reset();
  }

  submit() {
    if (this.refs['add-timesheet-form'].getWrappedInstance().submit()) {
      this.close();
    }
  }

  open() {
    this.setState({ showModal: true });
  }

  render() {
    return (
      <Col sm={1} smPull={1}>
        <Button bsStyle="primary" onClick={this.open.bind(this)}>
          Add Timesheet
        </Button>
        <Modal
          show={this.state.showModal}
          onHide={this.close.bind(this)}
          backdrop="static"
          enforceFocus={true}
          >
          <Modal.Header closeButton>
            <Modal.Title>Add New Timesheet</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <AddTimesheetForm ref="add-timesheet-form" />
          </Modal.Body>
          <Modal.Footer>
            <Button className="pull-left" onClick={this.close.bind(this)}>Cancel</Button>
            <Button className="pull-left" onClick={this.reset.bind(this)}>Reset</Button>
            <Button bsStyle="success" onClick={this.submit.bind(this)}>Add</Button>
          </Modal.Footer>
        </Modal>
      </Col>
    );
  }
}
