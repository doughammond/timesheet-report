// @flow

// data model
import Timesheet from './Data';


type State = {
  sheet?: Timesheet,
  minDate?: Date,
  maxDate?: Date,
  week_detail?: Object,
  pivotDimensions?: Array<Object>,
  loading?: boolean,
  loading_message?: string,

};

const defaultPivotDimensions  = [
  { value: 'classification', title: 'Classification' },
  // { value: 'category', title: 'Category' },
  // { value: 'description', title: 'Description' },
];

const initialState = {
  sheet: new Timesheet({}),
  minDate: null,
  maxDate: null,
  week_detail: {},
  pivotDimensions: defaultPivotDimensions
};

export function timesheetReducer(state:Object = initialState, action:Object): State {
  switch (action.type) {
    case 'REQUEST_STARTED':
      return {
        ...state,
        loading: true,
        loading_message: action.message || 'Loading...'
      };

    case 'DATA_LOADED':
      return {
        ...state,
        loading: false,
        loading_message: null,
        sheet: new Timesheet(action.data, /* user?! */)
      };

    case 'RANGES_LOADED':
      return {
        ...state,
        minDate: action.minDate,
        maxDate: action.maxDate
      }

    case 'TOGGLE_WEEK_DETAIL':
      return {
        ...state,
        week_detail: {
          ...state.week_detail,
          [action.week_code]: !state.week_detail[action.week_code]
        }
      }

    default:
      return state;
  }
};
