// @flow

export function pivotFilter(items: Array<Object>, dimensions: Array<Object>): Array<Object> {
  return items.filter(
    item => dimensions.every(dim => item[dim.value])
  );
};

function pivotReduce(memo: Object, row: Object, property: string): Object {
  // console.log('ReactPivot.reduce', row, memo);
  memo[property] = (memo[property] || 0) + (row[property] || 0);
  return memo;
};

export function calculatePivot(rows: Array<Object>, dimensions: Array<Object>, calculatedProperty: string): Array<Object> {
  const values = {};
  for (let dim of dimensions) {
    values[dim.value] = new Set(rows.map(
      row => row[dim.value]
    ));
  }

  const reduceOnPath = (propVals) => {
    let propPath = Object.keys(propVals);
    let filteredRows = rows
      .filter(
        row => propPath.every(key => row[key] === propVals[key])
      );
    return (filteredRows.length ? filteredRows : [{}])
      .reduce(
        (memo, row) => pivotReduce(memo, row, calculatedProperty),
        {}
      );
  };

  const createPivotObj = (propVals) => ({
    ...reduceOnPath(propVals),
    children: {}
  });

  const sortedValues = (key) => {
    return [...values[key]].sort();
  };

  let d1 = dimensions[0];
  let d2 = dimensions[1];
  const data = createPivotObj({});
  for (let dv1 of sortedValues(d1.value)) {
    let propVals = { [d1.value]: dv1 };
    data['children'][dv1] = createPivotObj(propVals);
    if (d2) {
      for (let dv2 of sortedValues(d2.value)) {
        propVals[d2.value] = dv2;
        data['children'][dv1]['children'][dv2] = createPivotObj(propVals);
      }
    }
  }

  return [data, values];
};

