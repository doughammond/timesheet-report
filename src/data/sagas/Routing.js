// @flow

import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import { paramsForUrl } from '../../Utils';


function* loadRouteData(action) {
  // console.debug('loadRouteData', action.payload.location.pathname);
  const params = paramsForUrl(action.payload.location.pathname);
  console.debug('loadRouteData params', params);
  const { type, year, week, quarter } = params;
  switch (type) {
     case 'year':
       yield put({
         type: 'TIMESHEETS_LOAD_YEAR_REQUEST',
         year
       });
       break;
     case 'quarter':
       yield put({
         type: 'TIMESHEETS_LOAD_QUARTER_REQUEST',
         year, quarter
       });
       break;
     case 'week':
       yield put({
         type: 'TIMESHEETS_LOAD_WEEK_REQUEST',
         year, week
       });
       break;
     default:
       break;
  }
}


function* changeLocation(action: Object): Object {
  yield put(push(action.url));
}


export function* RoutingSaga(): Object {
  yield takeEvery(
    'LOCATION_CHANGE_REQUEST',
    changeLocation
  );

  yield takeEvery(
    '@@router/LOCATION_CHANGE',
    loadRouteData
  );
}
