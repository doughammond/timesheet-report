// @flow

import moment from 'moment';
import { takeEvery, apply, put } from 'redux-saga/effects';

import { Client } from '../../client';
import { urlForWeek } from '../../Utils'

// FETCHING DATA

function* fetchTimesheets(action: Object): Object {
  try {
    let fn: Function,
        args: Array<number>;

    switch (action.type) {
      case 'TIMESHEETS_LOAD_YEAR_REQUEST':
        fn = Client.getTimesheetsByYear;
        args = [
          parseInt(action.year, 10)
        ];
        break;
      case 'TIMESHEETS_LOAD_QUARTER_REQUEST':
        fn = Client.getTimesheetsByQuarter;
        args = [
          parseInt(action.year, 10),
          parseInt(action.quarter, 10)
        ];
        break;
      case 'TIMESHEETS_LOAD_WEEK_REQUEST':
        fn = Client.getTimesheetsByWeek;
        args = [
          parseInt(action.year, 10),
          parseInt(action.week, 10)
        ];
        break;
      default:
        throw new Error('Invalid action');
    }
    yield put({
      type: 'REQUEST_STARTED'
    });
    // console.log('fetchTimesheets', action, fn, args);
    const timesheets = yield apply(Client, fn, args);
    yield put({
      type: 'DATA_LOADED',
      data: timesheets
    });
    // also update the main nav ranges
    yield put({
      type: 'RANGES_REQUEST'
    });
  } catch (e) {
    console.error('fetchTimesheets error', e);
    yield put({
      type: 'TIMESHEET_LOAD_FAILED',
      message: e.message
    });
  }
}

export function* TimesheetRequestSaga(): Object {
  yield takeEvery(
    [
      'TIMESHEETS_LOAD_YEAR_REQUEST',
      'TIMESHEETS_LOAD_QUARTER_REQUEST',
      'TIMESHEETS_LOAD_WEEK_REQUEST',
    ],
    fetchTimesheets
  );
}

// ADDING DATA

function* addTimesheet(action: Object): Object {
  try {
    yield put({
      type: 'REQUEST_STARTED',
      message: 'Saving...'
    });

    yield apply(Client, Client.importTimesheet, [action.date, action.items]);

    const md = moment(action.date);
    const tsYear = md.year();
    const tsWeek = md.isoWeek();
    yield put({
      type: 'LOCATION_CHANGE_REQUEST',
      url: urlForWeek(tsYear, tsWeek)
    })
  } catch (e) {
    console.error('addTimesheet error', e);
    yield put({
      type: 'TIMESHEET_ADD_FAILED',
      message: e.message,
      data: {
        date: action.date,
        items: action.items
      }
    });
  }
}

export function* TimesheetAddSaga(): Object {
  yield takeEvery(
    [
      'TIMESHEET_ADD_REQUEST'
    ],
    addTimesheet
  );
}
