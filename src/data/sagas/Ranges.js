// @flow

import { takeEvery, apply, put } from 'redux-saga/effects';

import { Client } from '../../client';


type _PartialTimesheetRecord = {
  min_item_time: string,
  max_item_time: string
};
type _MinMaxDates = {
  minDate: Date,
  maxDate: Date
};

function calculateMinMax(timesheets: Array<_PartialTimesheetRecord>): _MinMaxDates {
  const mins = [], maxs = [];
  timesheets.forEach(
    (sheet: _PartialTimesheetRecord) => {
      if (sheet.min_item_time) {
        mins.push(Date.parse(sheet.min_item_time));
      }
      if (sheet.max_item_time) {
        maxs.push(Date.parse(sheet.max_item_time));
      }
    }
  );
  return {
    minDate: new Date(Math.min(...mins)),
    maxDate: new Date(Math.max(...maxs))
  };
}

function* fetchRanges(action: Object): Object {
  try {
    // console.log('sagas/Ranges/fetchRanges timesheets req');
    const timesheets = yield apply(Client, Client.getAllTimesheets);
    // console.log('sagas/Ranges/fetchRanges timesheets', timesheets);
    yield put({
      type: 'RANGES_LOADED',
      ...calculateMinMax(timesheets)
    });
  } catch (e) {
    console.error('fetchRanges error', e);
    yield put({
      type: 'RANGES_FAILED',
      message: e.message
    });
  }
}

export function* RangesSaga(): Object {
  yield takeEvery(
    'RANGES_REQUEST',
    fetchRanges
  );
}

