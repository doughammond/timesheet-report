// @flow

import moment from 'moment';
import deepFreeze from 'deep-freeze';

import { calculateDistributions } from './Distribution';
import { pivotFilter, calculatePivot } from './Pivot';


class TimesheetItem {
  _time: Date;
  _duration: number;
  _description: string;
  _classification: string;
  _category: Array<string>;

  constructor(time: Date|string|number, duration: number, description: string, classification: string, category: Array<string>) {
    this._time = new Date(time);
    this._duration = duration;
    this._description = description;
    this._category = category;
    
    this._classification = 'Q4';
    const isIMP = category.indexOf('IMP') !== -1;
    const isURG = category.indexOf('URG') !== -1;
    if (isURG && isIMP) {
      this._classification = 'Q1';
    } else if (isIMP) {
      this._classification = 'Q2';
    } else if (isURG) {
      this._classification = 'Q3';
    }
  }

  get time(): Date {
    return this._time;
  }

  get start(): Date {
    return this._time;
  }

  get end(): Date {
    return new Date(+this._time + (this._duration * 60 * 1000));
  }

  get duration(): number {
    return this._duration;
  }

  get description(): string {
    return this._description;
  }

  get classification(): string {
    return this._classification;
  }

  get category(): Array<string> {
    return this._category;
  }
}

class TimesheetDay {
  _date: Date;
  _user: ?string;
  _items: Array<TimesheetItem>;
  _duration: number;

  constructor(date: Date|string|number, user: ?string, items: Array<Array<any>>) {
    this._date = new Date(date);
    this._user = user;
    this._items = items.sort(
      (a: Array<any>, b: Array<any>): number => a[0] - b[0]
    ).map(
      (item: Array<any>): TimesheetItem => new TimesheetItem(
        item[0],
        this._getLogItemDuration(items, item),
        item[1],
        item[2],
        item[3]
      )
    );
    this._duration = this._items.reduce(
      (total: number, item: TimesheetItem): number => total + item.duration,
      0
    );
  }

  _getLogItemDuration(allItems: Array<Array<any>>, item: Array<any>): number {
    const itemIndex = allItems.indexOf(item);
    const nextItem = allItems[itemIndex + 1] || item;
    return (nextItem[0].getTime() - item[0].getTime()) / (1000 * 60);
  }

  get title(): string {
    return moment(this._date).format('ddd D MMM YYYY');
  }

  get date(): Date {
    return this._date;
  }

  get user(): ?string {
    return this._user;
  }

  get week(): Array<number> {
    const md = moment(this._date);
    return [
      parseInt(md.format('GGGG'), 10),
      md.isoWeek()
    ];
  }

  get duration(): number {
    return this._duration;
  }

  get items(): Array<TimesheetItem> {
    return this._items;
  }

  get itemCount(): number {
    return this._items.length;
  }
}

export default class Timesheet {
  _sheet: Object;
  _user: ?string;
  _days: Array<TimesheetDay>;
  _weekdayStats: ?Object;
  _distribution: ?Object;
  _pivots: Object;
  _minDate: number;
  _maxDate: number;

  constructor(sheet: Object, user: ?string) {
    this._sheet = sheet;
    this._user = user;
    this._createDays();
    this._weekdayStats = null;
    this._distribution = null;
    this._pivots = {};
  }

  _createDays(): void {
    this._days = Object.keys(this._sheet).map(
      (date: string): TimesheetDay => new TimesheetDay(
        date,
        this._user,
        this._sheet[date] || []
      )
    );
    // now computed, this entire dataset is immutable
    deepFreeze(this._days);
    var dayKeys = this._days.map((d: TimesheetDay): Date => d.date);
    this._minDate = Math.min.apply(null, dayKeys);
    this._maxDate = Math.max.apply(null, dayKeys);
  }

  get sheet(): Object {
    return this._sheet;
  }

  get user(): ?string {
    return this._user;
  }

  get days(): Array<TimesheetDay> {
    return this._days;
  }

  get minDate(): number {
    return this._minDate;
  }

  get maxDate(): number {
    return this._maxDate;
  }

  get allData(): Array<TimesheetItem> {
    const days = this.days;
    let all = [];
    for (var i = 0, l = days.length; i < l; ++i) {
      for (var j = 0, m = days[i].items.length; j < m; ++j) {
        all.push(
          days[i].items[j]
        );
      }
    }
    return all;
  }

  get weekdayStats(): Object {
    if (!this._weekdayStats) {
      const daynames = [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
      ];
      const initial = daynames.reduce(
        (d: Object, dayname: string): Object => {
          d[dayname] = {
            'itemcount': 0,
            'duration': 0,
            'daycount': 0,
            'classifications': {
              // 'Q1': 0,
              // 'Q2': 0,
              // 'Q3': 0,
              // 'Q4': 0
            }
          };
          return d;
        },
        {}
      );
      const days = this.days;
      this._weekdayStats = days.reduce(
        (stats: Object, day: TimesheetDay): Object => {
          const weekday = moment(day.date).isoWeekday();
          const dayname = daynames[weekday - 1];
          stats[dayname]['daycount'] += 1;
          for (var item of day.items) {
            if (item.duration) {
              stats[dayname]['itemcount'] += 1;
              stats[dayname]['duration'] += item.duration;
              if (!(item.classification in stats[dayname]['classifications'])) {
                stats[dayname]['classifications'][item.classification] = 0
              }
              stats[dayname]['classifications'][item.classification] += item.duration;
            }
          }
          return stats;
        },
        initial
      );
    }
    return this._weekdayStats;
  }

  get distribution(): Object {
    if (!this._distribution) {
      this._distribution = deepFreeze(calculateDistributions(this));
    }
    return this._distribution;
  }

  calculatePivot(dimensions: Array<Object>, calculatedProperty: string): Object {
    // console.log('Timesheet.calculatePivot');
    const pivotKey = JSON.stringify(dimensions) + '--' + calculatedProperty;
    if (!(pivotKey in this._pivots)) {
      // console.log('Timesheet.calculatePivot set cache');
      this._pivots[pivotKey] = calculatePivot(
        pivotFilter(this.allData, dimensions),
        dimensions,
        calculatedProperty
      );
    }
    return this._pivots[pivotKey];
  }

  // http://bl.ocks.org/guypursey/620adb6185d7a6446f1ab34f9b579a1d
  get laneData(): Array<Object> {
    const lanes = [
      'DEV',
      'SUPP',
      'PRJ',
      'EML',
      'MEET',
      'INTR',
      'PHN',
      'SCRM',
    ];
    const lane_indices = lanes.reduce(
      (li, lane, i) => ({...li, [lane]:i }),
      {}
    );
    const lane_items = [];
    for (let item of this.allData) {
      let item_cat_set = new Set(item.category);
      for (let category of lanes) {
        if (item_cat_set.has(category)) {
          lane_items.push({
            id: lane_items.length,
            start: item.start,
            end: item.end,
            lane: lane_indices[category],
            classification: item.classification
          });
        }
      }
    }
    return [this.minDate, this.maxDate, lanes, lane_items];
  }

}
