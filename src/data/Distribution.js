// @flow

// TODO: fix nasty circular dependency for the Timesheet type definition
import Timesheet from './Data';

function _calculateSegmentDistribution(work: Array<Object>, resolution: number): Object {
  const distribution = {};
  for (let item of work) {
    let bucket = resolution * Math.ceil(item.duration / resolution);
    if (!(bucket in distribution)) {
      distribution[bucket] = 0;
    }
    distribution[bucket] += 1;
  }
  return distribution;
}

function _calculateTaskDurationDistribution(work: Array<Object>, resolution: number): Object {
  const distribution = {};
  const plannedWorkByTask = {};
  for (let item of work) {
    if (!(plannedWorkByTask[item.description])) {
      plannedWorkByTask[item.description] = 0;
    }
    plannedWorkByTask[item.description] += item.duration;
  }
  for (let taskKey of Object.keys(plannedWorkByTask)) {
    let bucket = resolution * Math.ceil(plannedWorkByTask[taskKey] / resolution);
    if (!(bucket in distribution)) {
      distribution[bucket] = 0;
    }
    distribution[bucket] += 1;
  }
  return distribution;
}

export function calculateDistributions(timesheet: Timesheet, resolution: number = 15): Object {
  const plannedWork = timesheet.allData.filter(
    row => row.category.includes('PRJ')
  );

  return {
    segments: _calculateSegmentDistribution(plannedWork, resolution),
    tasks:    _calculateTaskDurationDistribution(plannedWork, resolution)
  };
}
