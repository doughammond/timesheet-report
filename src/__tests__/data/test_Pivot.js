import { pivotFilter, calculatePivot } from '../../src/data/Pivot';

describe('pivotFilter', () => {
  it('ensures all items have values for all dimensions', () => {
    const items = [
      { d1: false, d2: false },
      { d1: false, d2: true },
      { d1: true, d2: false },
      { d1: true, d2: true },
    ];
    const dims = [
      { value: 'd1' },
      { value: 'd2' },
    ];
    const result = pivotFilter(items, dims);
    expect(result).toEqual([
      { d1: true, d2: true }
    ]);
  });
});

describe('calculatePivot', () => {
  it('works?', () => {
    const rows = [
      { d1: 'dv1', d2: 'dv2', value: 1 }
    ];
    const dims = [
      { value: 'd1' },
      { value: 'd2' },
    ];
    const calculatedProperty = 'value';
    const [pivot_data, pivot_values] = calculatePivot(rows, dims, calculatedProperty);
    expect(pivot_data).toEqual({
      children: {
        dv1: {
          children: {
            dv2: {
              children: {},
              value: 1
            }
          },
          value: 1
        }
      },
      value: 1
    });

    expect(pivot_values).toEqual({
      d1: new Set(['dv1']),
      d2: new Set(['dv2'])
    });
  });
});
