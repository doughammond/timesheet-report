jest.mock('../../src/data/Pivot');

import Timesheet from '../../src/data/Data';
import { calculatePivot } from '../../src/data/Pivot';

describe('Timesheet', () => {
  let sheet = {
    '2016-01-01': [
      [new Date('2016-01-01 09:00'), 'item 1', 'Q1', 'planned-work'],
      [new Date('2016-01-01 10:00'), 'item 2', 'Q2', 'planned-work'],
      [new Date('2016-01-01 11:00'), 'end']
    ]
  };
  const user = '__USER__';
  let ts;

  beforeEach(() => {
    ts = new Timesheet(
      sheet,
      user
    );
  });

  it('can initialise properties', () => {
    // yes, of course
    expect(ts instanceof Timesheet).toBe(true);
    // test the property getters
    expect(ts.sheet).toBe(sheet);
    expect(ts.user).toBe(user);
    expect(ts.days).toMatchSnapshot();
    expect(ts.minDate).toEqual(new Date(2016, 0, 1).getTime());
    expect(ts.maxDate).toEqual(new Date(2016, 0, 1).getTime());
    expect(ts.allData).toMatchSnapshot();
    expect(ts.weekdayStats).toMatchSnapshot();
    expect(ts.distribution).toEqual({
      segments: {
        '60': 2
      },
      tasks: {
        '60': 2
      }
    });
  });

  it('can calculate pivot', () => {
    calculatePivot.mockReturnValue('__PIVOT__');
    const pivot = ts.calculatePivot(
      [
        { value: 'category' },
        { value: 'classification' }
      ],
      'duration'
    );
    expect(pivot).toEqual('__PIVOT__');
  });

});

describe('TimesheetDay', () => {
  const sheet = {
    '2016-01-01': [
      [new Date('2016-01-01 09:00'), 'item 1', 'Q1', 'planned-work'],
      [new Date('2016-01-01 10:00'), 'item 2', 'Q2', 'planned-work'],
      [new Date('2016-01-01 11:00'), 'end']
    ]
  };

  let ts, day;
  beforeEach(() => {
    ts = new Timesheet(sheet, '__USER__');
    day = ts.days[0];
  });

  it('can initialise properties', () => {
    expect(day.title).toEqual('Fri 1 Jan 2016');
    expect(day.user).toEqual('__USER__');
    expect(day.week).toEqual([2015, 53]);
    expect(day.duration).toEqual(120);
    expect(day.itemCount).toEqual(3);
  });

  describe('TimesheetItem', () => {
    let item;
    beforeEach(() => {
      item = day.items[0];
    });

    it('can initialise properties', () => {
      expect(item.time).toEqual(new Date(2016, 0, 1, 9, 0));
      expect(item.duration).toEqual(60);
      expect(item.description).toEqual('item 1');
      expect(item.classification).toEqual('Q1');
      expect(item.category).toEqual('planned-work');
    });
  });

});
