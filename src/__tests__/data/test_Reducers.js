import { timesheetReducer } from '../../src/data/Reducers';

describe('timesheetReducer', () => {
  it('handles DATA_LOADED', () => {
    const action = {
      type: 'DATA_LOADED',
      data: {}
    };
    const beforeState = {};
    const afterState = timesheetReducer(beforeState, action);
    expect(afterState).toMatchSnapshot();
  });

  it('handles RANGES_LOADED', () => {
    const action = {
      type: 'RANGES_LOADED',
      minDate: '__MIN__',
      maxDate: '__MAX__'
    };
    const beforeState = {};
    const afterState = timesheetReducer(beforeState, action);
    expect(afterState).toMatchSnapshot();
  });

  it('handles TOGGLE_WEEK_DETAIL false to true', () => {
    const action = {
      type: 'TOGGLE_WEEK_DETAIL',
      week_code: 'code-001'
    };
    const beforeState = {
      week_detail: {
        'code-001': false
      }
    };
    const afterState = timesheetReducer(beforeState, action);
    expect(afterState).toMatchSnapshot();
  });

  it('handles TOGGLE_WEEK_DETAIL true to false', () => {
    const action = {
      type: 'TOGGLE_WEEK_DETAIL',
      week_code: 'code-001'
    };
    const beforeState = {
      week_detail: {
        'code-001': true
      }
    };
    const afterState = timesheetReducer(beforeState, action);
    expect(afterState).toMatchSnapshot();
  });

  it('returns the input for other actions', () => {
    const action = {
      type: 'FRUBBLE',
    };
    const beforeState = {
      my_delicate_state: '345kj3lj6'
    };
    const afterState = timesheetReducer(beforeState, action);
    expect(afterState).toEqual(beforeState)
  });
});
