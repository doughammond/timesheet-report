import Timesheet from '../../src/data/Data';

import { calculateDistributions } from '../../src/data/Distribution';

describe('calculateDistributions', () => {
  const user = '__USER__';
  let sheet, ts;
  beforeEach(() => {
    sheet = {};
    ts = null;
  });

  it('only counts planned-work', () => {
    sheet = {
      '2016-01-01': [
        [new Date('2016-01-01 09:00'), 's1', 'Q2', 'foo-bar'],
        [new Date('2016-01-01 10:00'), 's2', 'Q2', 'foo-bar'],
        [new Date('2016-01-01 11:00'), 's3', 'Q2', 'foo-bar'],
        [new Date('2016-01-01 12:00'), 'end']
      ]
    };
    ts = new Timesheet(
      sheet,
      user
    );
    expect(calculateDistributions(ts)).toEqual({
      segments: {},
      tasks: {}
    });
  });

  it('counts segment durations', () => {
    // 3 different tasks, 1 hr segments each
    sheet = {
      '2016-01-01': [
        [new Date('2016-01-01 09:00'), 's1', 'Q2', 'planned-work'],
        [new Date('2016-01-01 10:00'), 's2', 'Q2', 'planned-work'],
        [new Date('2016-01-01 11:00'), 's3', 'Q2', 'planned-work'],
        [new Date('2016-01-01 12:00'), 'end']
      ]
    };
    ts = new Timesheet(
      sheet,
      user
    );
    expect(calculateDistributions(ts)).toEqual({
      segments: {
        60: 3
      },
      tasks: {
        60: 3
      }
    });
  });

  it('counts unique task names', () => {
    // 1 task, 3x 1 hour long segments
    sheet = {
      '2016-01-01': [
        [new Date('2016-01-01 09:00'), 's1', 'Q2', 'planned-work'],
        [new Date('2016-01-01 10:00'), 's1', 'Q2', 'planned-work'],
        [new Date('2016-01-01 11:00'), 's1', 'Q2', 'planned-work'],
        [new Date('2016-01-01 12:00'), 'end']
      ]
    };
    ts = new Timesheet(
      sheet,
      user
    );
    expect(calculateDistributions(ts)).toEqual({
      segments: {
        60: 3
      },
      tasks: {
        180: 1
      }
    });
  });

  describe('calculating buckets according to resolution', () => {
    beforeEach(() => {
      sheet = {
        '2016-01-01': [
          [new Date('2016-01-01 09:00'), '10 mins', 'Q2', 'planned-work'],
          [new Date('2016-01-01 09:10'), '20 mins', 'Q2', 'planned-work'],
          [new Date('2016-01-01 09:30'), '30 mins', 'Q2', 'planned-work'],
          [new Date('2016-01-01 10:00'), 'end']
        ]
      };
      ts = new Timesheet(
        sheet,
        user
      );
    });

    it('calculates to 10 minutes', () => {
      expect(calculateDistributions(ts, 10)).toEqual({
        segments: {
          10: 1,
          20: 1,
          30: 1,
        },
        tasks: {
          10: 1,
          20: 1,
          30: 1,
        },
      });
    });

    it('calculates to 20 minutes', () => {
      expect(calculateDistributions(ts, 20)).toEqual({
        segments: {
          20: 2,
          40: 1
        },
        tasks: {
          20: 2,
          40: 1
        }
      });
    });

    it('calculates to 30 minutes', () => {
      expect(calculateDistributions(ts, 30)).toEqual({
        segments: {
          30: 3,
        },
        tasks: {
          30: 3
        }
      });
    });
  });
});
