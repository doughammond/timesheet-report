import Promise from 'bluebird';
import { Importer } from '../../src/db/import';

describe('Importer', () => {
  let ts, db, imp;
  beforeEach(() => {
    ts = {
      days: []
    };
    db = {
      getAllTimesheets: jest.fn().mockReturnValue(
        Promise.resolve([])
      ),
      getTimesheetsByNameForUser: jest.fn().mockReturnValue(
        Promise.resolve([])
      ),
      getActiveUsers: jest.fn().mockReturnValue(
        Promise.resolve([])
      ),
      insertUser: jest.fn().mockReturnValue(
        Promise.resolve({
          changes: [
            { new_val: 'user-name' }
          ]
        })
      ),
      insertTimesheet: jest.fn().mockReturnValue(
        Promise.resolve({
          generated_keys: [111]
        })
      )
    };
    imp = new Importer(ts, db);
  });

  it('can initialise properties', () => {
    expect(imp.db).toBe(db);
  });

  it('can run the import', () => {
    ts.days = [
      { title: 'foo', items: [  ] }
    ];
    return  imp.run().then(
      result => {
        expect(result).toBe(imp);
      }
    );
  });

  it('can calculate a timesheet min/max date', () => {
    db.getTimesheetItemsByTimesheetId = jest.fn().mockReturnValue(
      Promise.resolve([
        { time_started: new Date(2016, 1, 1) },
        { time_started: new Date(2016, 0, 1) },
      ])
    );
    db.updateTimesheet = jest.fn().mockReturnValue(
      Promise.resolve('__FINAL_VALUE__')
    );

    const timesheet = {
      id: 1111
    };
    return imp._calculateTimesheetMinMax(timesheet).then(
      result => {
        expect(result).toEqual('__FINAL_VALUE__');
        expect(timesheet.min_item_time).toEqual(new Date(2016, 0, 1));
        expect(timesheet.max_item_time).toEqual(new Date(2016, 1, 1));
      }
    );
  });

  it('can create a timesheet item', () => {
    db.insertTimesheetItem = jest.fn().mockReturnValue(
      Promise.resolve({
        generated_keys: [1111]
      })
    );
    imp.updateItem = jest.fn().mockImplementation(
      (dbitem, item) => dbitem
    );

    const sheet = {
      id: '__TIMESHEET_ID__'
    };
    const item = {
      description: '__DESCRIPTION__',
      time: '__TIME__',
      classification: '__CLASSIFICATION__',
      category: '__CATEGORY__'
    };
    return imp.createItem(sheet, item).then(
      result => {
        expect(result).toMatchSnapshot();
      }
    );
  });

  it('can create a timesheet', () => {
    db.insertTimesheet = jest.fn().mockReturnValue(
      Promise.resolve({
        generated_keys: [1111]
      })
    );
    imp.updateSheet = jest.fn().mockImplementation(
      (sheet, day, user_id) => sheet
    );

    const day = {
      title: '__TITLE__',
      date: '__DATE__',
      items: []
    };
    const user_id = '__USER_ID__';
    const now = new Date(2016, 10, 1);
    return imp.createSheet(day, user_id, now).then(
      result => {
        expect(result).toMatchSnapshot();
      }
    );
  });

  it('can update a timesheet item', () => {
    db.updateTimesheetItem = jest.fn().mockImplementation(
      (dbitem) => Promise.resolve(dbitem)
    );

    const dbitem = {};
    const item = {
      time: '__TIME__',
      description: '__DESCRIPTION__',
      classification: '__CLASSIFICATION__',
      category: '__CATEGORY__'
    };
    return imp.updateItem(dbitem, item).then(
      result => {
        expect(result).toBe(dbitem);
        expect(dbitem).toMatchSnapshot();
      }
    );
  });

  it('can update a timesheet', () => {
    imp.detectItem = jest.fn().mockImplementation(
      (sheet, item) => Promise.resolve(item)
    );

    const sheet = {};
    const day = {
      items: [
        '__FIRST__',
        '__SECOND__'
      ]
    };
    const user_id = '__USER_ID__';
    return imp.updateSheet(sheet, day, user_id).then(
      result => {
        expect(result).toEqual('__SECOND__');
      }
    );
  });
});
