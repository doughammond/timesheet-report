import { Database } from '../../src/db';

describe('Database', () => {
  let cfg, rth, confn, con, db, cursor;
  beforeEach(() => {
    cfg = {};

    // some sort of rethink mock
    rth = {};
    rth.table = jest.fn().mockReturnValue(rth);
    rth.insert = jest.fn().mockReturnValue(rth);
    rth.get = jest.fn().mockReturnValue(rth);
    rth.update = jest.fn().mockReturnValue(rth);
    rth.orderBy = jest.fn().mockReturnValue(rth);
    rth.filter = jest.fn().mockReturnValue(rth);
    rth.row = jest.fn().mockReturnValue(rth);
    rth.eq = jest.fn().mockReturnValue(rth);
    rth.time = jest.fn();
    cursor = {
      toArray: jest.fn()
    };
    rth.run = jest.fn().mockReturnValue(Promise.resolve(cursor));

    con = {
      close: jest.fn()
    };
    confn = jest.fn().mockReturnValue(
      Promise.resolve(con)
    );
    db = new Database(cfg, rth, confn);
  });

  it('initialises connection', () => {
    const c = db.connection;
    return c.then(
      connection => {
        expect(connection).toEqual(con);
      }
    );
  });

  it('can close connection', () => {
    con.close.mockReturnValue('__CLOSE__');
    return db.connection.then(
      conn => {
        const r = db.close();
        expect(r).toEqual('__CLOSE__');
        expect(db._conn).toBe(null);
      }
    );
  });

  it('can insert user', () => {
    const r = db.insertUser('user-name');
    return r.then(
      result => {
        expect(result).toEqual(cursor);
      }
    );
  });

  it('can insert timesheet', () => {
    const r = db.insertTimesheet({});
    return r.then(
      result => {
        expect(result).toEqual(cursor);
      }
    );
  });

  it('can insert timesheet item', () => {
    const r = db.insertTimesheetItem({});
    return r.then(
      result => {
        expect(result).toEqual(cursor);
      }
    );
  });

  it('can update timesheet', () => {
    const r = db.updateTimesheet({});
    return r.then(
      result => {
        expect(result).toEqual(cursor);
      }
    );
  });

  it('can update timesheet item', () => {
    const r = db.updateTimesheetItem({});
    return r.then(
      result => {
        expect(result).toEqual(cursor);
      }
    );
  });

  it('can get all timesheets', () => {
    cursor.toArray.mockReturnValue(['ts']);
    const r = db.getAllTimesheets();
    return r.then(
      result => {
        expect(result).toEqual(['ts']);
      }
    );
  });

  it('can get all timesheet items', () => {
    cursor.toArray.mockReturnValue(['tsi']);
    const r = db.getAllTimesheetItems();
    return r.then(
      result => {
        expect(result).toEqual(['tsi']);
      }
    );
  });

  it('can get timesheets by year', () => {
    cursor.toArray.mockReturnValue(['ts']);
    const r = db.getTimesheetsByYear(2016);
    return r.then(
      result => {
        expect(result).toEqual(['ts']);
      }
    );
  });

  it('can get timesheets by quarter', () => {
    cursor.toArray.mockReturnValue(['ts']);
    const r = db.getTimesheetsByQuarter(2016, 1);
    return r.then(
      result => {
        expect(result).toEqual(['ts']);
      }
    );
  });

  it('can get timesheets by week', () => {
    cursor.toArray.mockReturnValue(['ts']);
    const r = db.getTimesheetsByWeek(2016, 1);
    return r.then(
      result => {
        expect(result).toEqual(['ts']);
      }
    );
  });

  it('can get timesheets by name for user', () => {
    cursor.toArray.mockReturnValue(['ts']);
    const r = db.getTimesheetsByNameForUser('name', 'user');
    return r.then(
      result => {
        expect(result).toEqual(['ts']);
      }
    );
  });

  it('can get timesheet items by timesheet id', () => {
    cursor.toArray.mockReturnValue(['ts']);
    const r = db.getTimesheetItemsByTimesheetId(1111);
    return r.then(
      result => {
        expect(result).toEqual(['ts']);
      }
    );
  });

  it('can get timesheet items by timesheet id and time started', () => {
    cursor.toArray.mockReturnValue(['ts']);
    const r = db.getTimesheetItemsByTimesheetIdAndTimeStarted(1111, new Date(2016, 0, 1, 9, 0));
    return r.then(
      result => {
        expect(result).toEqual(['ts']);
      }
    );
  });

  it('can get active users', () => {
    cursor.toArray.mockReturnValue(['user']);
    const r = db.getActiveUsers();
    return r.then(
      result => {
        expect(result).toEqual(['user']);
      }
    );
  });
});
