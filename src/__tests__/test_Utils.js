import {
  dayFormatter,
  timeFormatter,
  durationFormatter,
  splitWorkTimeWithOT,
  urlForYear,
  urlForQuarter,
  urlForWeek,
  titleForYear,
  titleForQuarter,
  titleForWeek,
  titleForUrl
} from '../src/Utils';


describe('dayFormatter', () => {
  it('formats a date into a day', () => {
    const result = dayFormatter(new Date(2016, 0, 1, 9, 0));
    expect(result).toEqual('Fri, Jan 1, 2016');
  });

  it('returns empty strings for Infinity input', () => {
    const result = dayFormatter(Infinity);
    expect(result).toEqual('');
  });

  it('returns empty strings for -Infinity input', () => {
    const result = dayFormatter(-Infinity);
    expect(result).toEqual('');
  });

  it('returns non date parseable inputs as output', () => {
    const strInput = '__SOME_STRING__';
    const result = dayFormatter(strInput);
    expect(result).toBe(strInput);
  });
});

describe('timeFormatter', () => {
  it('formats a date into a time', () => {
    const result = timeFormatter(new Date(2016, 0, 1, 9, 0));
    expect(result).toEqual('9:00 AM');
  });

  it('returns empty strings for Infinity input', () => {
    const result = timeFormatter(Infinity);
    expect(result).toEqual('');
  });

  it('returns empty strings for -Infinity input', () => {
    const result = timeFormatter(-Infinity);
    expect(result).toEqual('');
  });

  it('returns non date parseable inputs as output', () => {
    const strInput = '__SOME_STRING__';
    const result = timeFormatter(strInput);
    expect(result).toBe(strInput);
  });
});

describe('durationFormatter', () => {
  describe('default configuration', () => {
    const minsPerHour = 60, minsPerDay = 8*60, minsPerWeek = 5*8*60;

    it('returns "m" format for duration < one hour', () => {
      const result = durationFormatter(13);
      expect(result).toEqual('13m');
    });

    it('returns "h m" format for one hour < duration < one day', () => {
      const result = durationFormatter(13 + 4*minsPerHour);
      expect(result).toEqual('4h 13m');
    });

    it('returns "d h m" format for one day < duration < one week', () => {
      const result = durationFormatter(13 + 4*minsPerHour + 2*minsPerDay);
      expect(result).toEqual('2d 4h 13m');
    });

    it('returns "d m" format for one day < duration < one week but duration is less than one hour into a day', () => {
      const result = durationFormatter(13 + 0*minsPerHour + 2*minsPerDay);
      expect(result).toEqual('2d 13m');
    });

    it('returns "w d h m" format for one week < duration', () => {
      const result = durationFormatter(13 + 4*minsPerHour + 2*minsPerDay + 3*minsPerWeek);
      expect(result).toEqual('3w 2d 4h 13m');
    });

    it('returns "w h m" format for one week < duration but duration is less than one day into a week', () => {
      const result = durationFormatter(13 + 4*minsPerHour + 0*minsPerDay + 3*minsPerWeek);
      expect(result).toEqual('3w 4h 13m');
    });

    it('returns "w d m" format for one week < duration but duration is less than one hour into a day', () => {
      const result = durationFormatter(13 + 0*minsPerHour + 2*minsPerDay + 3*minsPerWeek);
      expect(result).toEqual('3w 2d 13m');
    });

    it('returns "w m" format for one week < duration but duration is less than one day into a week and less than one hour into a day', () => {
      const result = durationFormatter(13 + 0*minsPerHour + 0*minsPerDay + 3*minsPerWeek);
      expect(result).toEqual('3w 13m');
    });
  });
});

describe('splitWorkTimeWithOT', () => {
  it('returns remainder after limit', () => {
    const result = splitWorkTimeWithOT(100, 0, 75);
    expect(result).toEqual([100, 25]);
  });

  it('returns remainder after subtracting non work', () => {
    const result = splitWorkTimeWithOT(100, 10, 75);
    expect(result).toEqual([90, 15]);
  });
});

describe('urlForYear', () => {
  it('does some simple string formatting', () => {
    const result = urlForYear(2016);
    expect(result).toEqual('/year/2016');
  });
});

describe('urlForQuarter', () => {
  it('does some simple string formatting', () => {
    const result = urlForQuarter(2016, 1);
    expect(result).toEqual('/quarter/2016/1');
  });
});

describe('urlForWeek', () => {
  it('does some simple string formatting', () => {
    const result = urlForWeek(2016, 1);
    expect(result).toEqual('/week/2016/1');
  });
});

describe('titleForYear', () => {
  it('does some simple string formatting', () => {
    const result = titleForYear(2016);
    expect(result).toEqual('2016');
  });
});

describe('titleForQuarter', () => {
  it('does some simple string formatting', () => {
    const result = titleForQuarter(2016, 1);
    expect(result).toEqual('2016 Q1');
  });
});

describe('titleForWeek', () => {
  it('does some simple string formatting', () => {
    const result = titleForWeek(2016, 1);
    expect(result).toEqual('2016 CW 1');
  });
});

describe('titleForUrl', () => {
  it('works for a /year url', () => {
    const result = titleForUrl('/year/2016');
    expect(result).toEqual('2016');
  });
  it('works for a /quarter url', () => {
    const result = titleForUrl('/quarter/2016/1');
    expect(result).toEqual('2016 Q1');
  });
  it('works for a /week url', () => {
    const result = titleForUrl('/week/2016/1');
    expect(result).toEqual('2016 CW 1');
  });
  it('returns an empty string for other input', () => {
    const result = titleForUrl('foo bar');
    expect(result).toEqual('');
  });
});
