// react-dom/react-test-renderer work-around; see https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

// cannot correctly render chartjs elements :/
jest.mock('react-chartjs-2');

import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import { TimesheetReport } from '../../../src/components/TimesheetReport';
import Timesheet from '../../../src/data/Data';


describe('TimesheetReport', () => {
  it('renders warning message if data is empty', () => {
    const sheet = {
      allData: []
    };
    const store = configureMockStore([])({
      timesheets: {
        sheet
      }
    });
    const component = renderer.create(
      <Provider store={store}>
        <TimesheetReport />
      </Provider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders the full shebang when given data', () => {
    const sheet = new Timesheet(
      {
        '2016-01-01': [
          [new Date('2016-01-01 09:00'), 'item 1', 'Q1', 'planned-work'],
          [new Date('2016-01-01 10:00'), 'item 2', 'Q2', 'planned-work'],
          [new Date('2016-01-01 11:00'), 'end']
        ]
      },
      'doug-h'
    );
    const store = configureMockStore([])({
      timesheets: {
        sheet,
        pivotDimensions: [
          { value: 'classification', title: 'Classification' },
          { value: 'category', title: 'Category' }
        ]
      }
    });
    const component = renderer.create(
      <Provider store={store}>
        <TimesheetReport />
      </Provider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
