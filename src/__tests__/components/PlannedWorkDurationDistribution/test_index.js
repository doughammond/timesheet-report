// react-dom/react-test-renderer work-around; see https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

// cannot correctly render chartjs elements :/
jest.mock('react-chartjs-2');

import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import {
  _calculate_ClassificationPieChart,
  _calculate_ClassificationsPerDay,
  _calculate_LengthSegmentsDurationPerDay,
  _calculate_PlannedWorkDistribution,
  PlannedWorkDurationDistribution
} from '../../../src/components/PlannedWorkDurationDistribution';


describe('_calculate_ClassificationPieChart', () => {
  const pivotData = {
    'children': {
      'Q1': {
        'children': {},
        'duration': 1
      },
      'Q2': {
        'children': {},
        'duration': 3
      }
    },
    'duration': 4
  };
  const pivotValues = {
    classification: ['Q1', 'Q2']
  };

  it('constructs data set based on pivot data', () => {
    const result = _calculate_ClassificationPieChart(pivotData, pivotValues);
    expect(result).toMatchSnapshot();
  });
});

describe('_calculate_ClassificationsPerDay', () => {
  const weekdayStats = {
    'Monday': {
      'itemcount': 1,
      'duration': 4,
      'daycount': 1,
      'classifications': {
        'Q1': 1,
        'Q2': 1,
        'Q3': 1,
        'Q4': 1
      }
    },
    'Tuesday': {
      'itemcount': 2,
      'duration': 3,
      'daycount': 1,
      'classifications': {
        'Q1': 1,
        'Q2': 1,
        'Q3': 1,
        'Q4': 0
      }
    }
  };

  it('constructs data set based on weekdayStats', () => {
    const result = _calculate_ClassificationsPerDay(weekdayStats);
    expect(result).toMatchSnapshot();
  });
});

describe('_calculate_LengthSegmentsDurationPerDay', () => {
  const weekdayStats = {
    'Monday': {
      'itemcount': 1,
      'duration': 2,
      'daycount': 1,
      'classifications': {
        'Q1': 1
      }
    },
    'Tuesday': {
      'itemcount': 2,
      'duration': 3,
      'daycount': 1,
      'classifications': {
        'Q2': 1
      }
    }
  };

  it('constructs data set based on weekdayStats', () => {
    const result = _calculate_LengthSegmentsDurationPerDay(weekdayStats);
    expect(result).toMatchSnapshot();
  });
});

describe('_calculate_PlannedWorkDistribution', () => {
  const distribution = {
    'segments': {
      15: 6,
      30: 2
    },
    'tasks': {
      15: 1,
      30: 1,
      45: 1,
      60: 1
    }
  };
  it('constructs data set based on distribution', () => {
    const result = _calculate_PlannedWorkDistribution(distribution);
    expect(result).toMatchSnapshot();
  });
});

describe('PlannedWorkDurationDistribution', () => {
  it('renders data based on redux state', () => {
    const store = configureMockStore([])({
      timesheets: {
        sheet: {
          weekdayStats: {
            'Monday': {
              'itemcount': 1,
              'duration': 4,
              'daycount': 1,
              'classifications': {
                'Q1': 1,
                'Q2': 1,
                'Q3': 1,
                'Q4': 1
              }
            },
            'Tuesday': {
              'itemcount': 2,
              'duration': 3,
              'daycount': 1,
              'classifications': {
                'Q1': 1,
                'Q2': 1,
                'Q3': 1,
                'Q4': 0
              }
            }
          },
          distribution: {
            'segments': {
              15: 6,
              30: 2
            },
            'tasks': {
              15: 1,
              30: 1,
              45: 1,
              60: 1
            }
          },
          calculatePivot: function() {
            const pivotData = {
              'children': {
                'Q1': {
                  'children': {},
                  'duration': 1
                },
                'Q2': {
                  'children': {},
                  'duration': 3
                }
              },
              'duration': 4
            };
            const pivotValues = {
              classification: ['Q1', 'Q2']
            };
            return [pivotData, pivotValues];
          }
        }
      }
    });
    const component = renderer.create(
      <Provider store={store}>
        <PlannedWorkDurationDistribution />
      </Provider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
