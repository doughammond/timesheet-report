// react-dom/react-test-renderer work-around; see https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');
jest.mock('dom-helpers/util/inDOM', () => false);
// due to this issue we cannot actually render the Modal in the tests below :(


// don't test/render the form subcomponent
jest.mock('../../../src/components/AddTimesheetForm');

import React from 'react';
import renderer from 'react-test-renderer';

import { AddTimesheetDialog } from '../../../src/components/AddTimesheetDialog';

describe('AddTimesheetDialog', () => {
  let component;
  beforeEach(() => {
    component = renderer.create(
      <AddTimesheetDialog />
    );
  });
  it('renders a Button in default state', () => {
    // first render in default state
    expect(component.getInstance().state).toEqual({
      showModal: false
    });
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders a Button and Modal in open state', () => {
    // and then with Modal open
    component.getInstance().open();
    expect(component.getInstance().state).toEqual({
      showModal: true
    });
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders a Button in closed state', () => {
    // then close again
    component.getInstance().close();
    expect(component.getInstance().state).toEqual({
      showModal: false
    });
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
