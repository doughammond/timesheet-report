// react-dom/react-test-renderer work-around; see https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

// do not render sub components
jest.mock('../../../src/components/AddTimesheetDialog');


import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import { Header } from '../../../src/components/Header';

describe('Header', () => {
  it('renders some data based on redux state', () => {
    const store = configureMockStore([])({
      timesheets: {
        sheet: {
          minDate: new Date(2016, 0, 1),
          maxDate: new Date(2016, 1, 1)
        }
      }
    });
    const component = renderer.create(
      <Provider store={store}>
        <Header>
          <div id="test-child-component" />
        </Header>
      </Provider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
