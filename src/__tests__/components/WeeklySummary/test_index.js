// react-dom/react-test-renderer work-around; see https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import { WeeklySummary } from '../../../src/components/WeeklySummary';

describe('WeeklySummary', () => {
  it('renders some data based on redux state', () => {
    const sheet = {
      days: [
        {
          week: [2016, 1],
          duration: 4,
          items: [],
          date: new Date(2016, 0, 3)
        }
      ]
    };
    const store = configureMockStore([])({
      timesheets: {
        week_detail: {}
      }
    });
    const component = renderer.create(
      <Provider store={store}>
        <WeeklySummary sheet={sheet} />
      </Provider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
