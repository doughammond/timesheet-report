// react-dom/react-test-renderer work-around; see https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import { PivotTable } from '../../../src/components/PivotTable';

describe('PivotTable', () => {
  it('renders empty table with no pivotData', () => {
    const store = configureMockStore([])({
      timesheets: {
        sheet: {
          calculatePivot: () => [
            // props.pivotData
            null,
            // props.pivotValues
            {}
          ]
        },
        pivotDimensions: [
          { value: 'classification', title: 'Classification' },
          { value: 'category', title: 'Category' }
        ]
      }
    });
    const component = renderer.create(
      <Provider store={store}>
        <PivotTable />
      </Provider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders pivot table with complete props', () => {
    const store = configureMockStore([])({
      timesheets: {
        sheet: {
          calculatePivot: () => [
            // props.pivotData
            {
              children: {
                'Q1': {
                  children: {
                    'planned-work': {
                      children: {},
                      // omitting duration here causes the DurationCell
                      // component to render an empty div
                    }
                  },
                  duration: 1
                },
              },
              duration: 1
            },
            // props.pivotValues
            {
              classification: new Set(['Q1']),
              category: new Set(['planned-work'])
            }
          ]
        },
        pivotDimensions: [
          { value: 'classification', title: 'Classification' },
          { value: 'category', title: 'Category' }
        ]
      }
    });
    const component = renderer.create(
      <Provider store={store}>
        <PivotTable />
      </Provider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
