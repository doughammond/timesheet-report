// react-dom/react-test-renderer work-around; see https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import {
  _validateRows,
  AddTimesheetForm
} from '../../../src/components/AddTimesheetForm';


describe('_validateRows', () => {
  it('marks empty row object as invalid', () => {
    const rows = [
      {}
    ];
    const result = _validateRows(rows);
    expect(result).toEqual(false);
    expect(rows).toEqual([
      { _valid: 'error' }
    ]);
  });

  it('marks non-empty description and formatted time as valid', () => {
    const rows = [
      { description: 'foo', time: '09:00' }
    ];
    const result = _validateRows(rows);
    expect(result).toEqual(true);
    expect(rows).toEqual([
      { description: 'foo', time: '09:00', _valid: undefined }
    ]);
  });

  it('marks non-empty description and formatted time as valid and ignores other properties', () => {
    const rows = [
      { description: 'foo', time: '09:00', bar: 1 }
    ];
    const result = _validateRows(rows);
    expect(result).toEqual(true);
    expect(rows).toEqual([
      { description: 'foo', time: '09:00', bar: 1, _valid: undefined }
    ]);
  });

  it('marks empty description and formatted time as invalid', () => {
    const rows = [
      { description: '', time: '09:00' }
    ];
    const result = _validateRows(rows);
    expect(result).toEqual(false);
    expect(rows).toEqual([
      { description: '', time: '09:00', _valid: 'error' }
    ]);
  });

  it('marks non-empty description and non-formatted time as invalid', () => {
    const rows = [
      { description: 'foo', time: 'abcd' }
    ];
    const result = _validateRows(rows);
    expect(result).toEqual(false);
    expect(rows).toEqual([
      { description: 'foo', time: 'abcd', _valid: 'error' }
    ]);
  });


});

describe('AddTimesheetForm', () => {
  it('renders a form', () => {
    const store = configureMockStore([])({
      formDate: new Date(2016, 0, 1)
    });
    const component = renderer.create(
      <Provider store={store}>
        <AddTimesheetForm />
      </Provider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
