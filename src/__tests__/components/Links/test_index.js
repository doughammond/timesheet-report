// react-dom/react-test-renderer work-around; see https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import {
  generateYearQuarterWeeks,
  LinkSetQuarter,
  LinkSetYear,
  LinkSet,
  Links
} from '../../../components/Links';

describe('generateYearQuarterWeeks', () => {
  it('returns empty object if start > end', () => {
    const result = generateYearQuarterWeeks(
      new Date(2016, 1, 1),
      new Date(2016, 0, 1)
    );
    expect(result).toEqual({});
  });

  it('returns empty object if start == end', () => {
    const result = generateYearQuarterWeeks(
      new Date(2016, 0, 1),
      new Date(2016, 0, 1)
    );
    expect(result).toEqual({});
  });

  it('returns nested year/quarter/week items if start < end', () => {
    const result = generateYearQuarterWeeks(
      new Date(2016, 0, 1),
      new Date(2016, 1, 1)
    );
    expect(result).toEqual({
      2016: {
        1: [
          53, 1, 2, 3, 4
        ]
      }
    });
  });

  it('returns single nested year/quarter/week item if start negligably < end', () => {
    const result = generateYearQuarterWeeks(
      new Date(2016, 0, 1),
      new Date(2016, 0, 1, 0, 1)
    );
    expect(result).toEqual({
      2016: {
        1: [
          53
        ]
      }
    });
  });
});

describe('LinkSetQuarter', () => {
  it('renders some data', () => {
    const component = renderer.create(
      <LinkSetQuarter
        year={2016}
        quarter={1}
        weeks={[53, 1]}
      />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('LinkSetYear', () => {
  it('renders some data', () => {
    const component = renderer.create(
      <LinkSetYear
        year={2016}
        yearData={{1: [53, 1]}}
      />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('LinkSet', () => {
  it('renders some data', () => {
    const component = renderer.create(
      <LinkSet
        minDate={new Date(2016, 0, 1)}
        maxDate={new Date(2016, 0, 1, 0, 1)}
      />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('Links', () => {
  it('renders data based on redux state', () => {
    const store = configureMockStore([])({
      timesheets: {
        minDate: new Date(2016, 0, 1),
        maxDate: new Date(2016, 0, 1, 0, 1)
      }
    });
    const component = renderer.create(
      <Provider store={store}>
        <Links />
      </Provider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
