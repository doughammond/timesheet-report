jest.mock('../../src/db');

import { TimesheetsClient } from '../../src/client';

describe('Client', () => {
  let Client;
  beforeEach(() => {
    Client = new TimesheetsClient();
    Client._db = {};
  });

  it('is an object', () => {
    expect(Client).toMatchSnapshot();
  });

  it('can map items to model data', () => {
    const items = [
      {
        time_started: new Date(2016, 0, 1),
        description: '_description',
        categories: {
          classification: '_classification',
          type: '_type'
        }
      },
      {
        time_started: new Date(2016, 0, 2),
        description: '_description',
        categories: {
          classification: '_classification',
          type: '_type'
        }
      }
    ];
    const modelData = Client._mapTimesheetItemsToModelData(items);
    expect(modelData).toMatchSnapshot();
  });

  it('can import timesheet', () => {
    Client._db.importTimesheet = jest.fn().mockReturnValue(
      Promise.resolve({})
    );

    const d = new Date(2016, 0, 1);
    const items = [
      {}
    ];
    const add = Client.importTimesheet(d, items);
    return add.then(
      result => {
        expect(result).toMatchSnapshot()
      }
    );
  });

  it('can get all timesheets', () => {
    Client._db.getAllTimesheets = jest.fn().mockReturnValue(
      Promise.resolve([])
    );

    const records = Client.getAllTimesheets();
    return records.then(
      result => {
        expect(result).toEqual([])
      }
    );
  });

  it('can get all timesheet items', () => {
    Client._db.getAllTimesheetItems = jest.fn().mockReturnValue(
      Promise.resolve([])
    );

    const records = Client.getAllTimesheetItems();
    return records.then(
      result => {
        expect(result).toEqual({})
      }
    );
  });

  it('can get timesheets by year', () => {
    Client._db.getTimesheetsByYear = jest.fn().mockReturnValue(
      Promise.resolve([])
    );

    const records = Client.getTimesheetsByYear(2016);
    return records.then(
      result => {
        expect(result).toEqual({})
      }
    );
  });

  it('can get timesheets by quarter', () => {
    Client._db.getTimesheetsByQuarter = jest.fn().mockReturnValue(
      Promise.resolve([])
    );

    const records = Client.getTimesheetsByQuarter(2016, 1);
    return records.then(
      result => {
        expect(result).toEqual({});
      }
    );
  });

  it('can get timesheets by week', () => {
    Client._db.getTimesheetsByWeek = jest.fn().mockReturnValue(
      Promise.resolve([])
    );

    const records = Client.getTimesheetsByWeek(2016, 1);
    return records.then(
      result => {
        expect(result).toEqual({});
      }
    );
  });

});
